// src/lambda.ts
import { Context, Handler } from 'aws-lambda';
import { Server } from 'http';
import { proxy } from 'aws-serverless-express';
import { bootstrap } from './main';

// NOTE: If you get ERR_CONTENT_DECODING_FAILED in your browser, this is likely
// due to a compressed response (e.g. gzip) which has not been handled correctly
// by aws-serverless-express and/or API Gateway. Add the necessary MIME types to
// binaryMimeTypes below

let cachedServer: Server;

export const handler: Handler = async (event: any, context: Context) => {
  cachedServer = await bootstrap(true);
  return proxy(cachedServer, event, context, 'PROMISE').promise;
};
