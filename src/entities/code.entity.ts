import { Entity, Property } from '@mikro-orm/core';
import { CommonEntity } from './common.entity';

@Entity()
export class Code extends CommonEntity {
  @Property()
  code: string;

  @Property()
  code_kor_nm: string;

  @Property()
  code_desc: string;
}
