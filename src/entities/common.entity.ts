import { Entity, PrimaryKey, SerializedPrimaryKey } from '@mikro-orm/core';
import { ObjectId } from '@mikro-orm/mongodb';

@Entity({ abstract: true })
export abstract class CommonEntity {
  @PrimaryKey()
  _id: ObjectId;

  @SerializedPrimaryKey()
  id!: string;

  // @Enum(() => UseYnEnum)
  // use_yn: UseYnEnum = UseYnEnum.Y;

  // @Property()
  // created_by: string;
  //
  // @Property({
  //   onCreate: () => dayjs().toDate(),
  //   type: () => Dayjs,
  // })
  // created_at: Dayjs;
  //
  // @Property({ nullable: true })
  // updated_by = '';
  //
  // @Property({
  //   onUpdate: () => dayjs().toDate(),
  //   type: () => Dayjs,
  //   nullable: true,
  // })
  // updated_at: Dayjs = null;
  //
  // @Property({ nullable: true })
  // deleted_by = '';
  //
  // @Property({ type: () => Dayjs, nullable: true })
  // deleted_at: Dayjs = null;
  //
  // @BeforeUpdate()
  // setUpdateBy() {
  //   if (!this.updated_by && this.created_by) this.updated_by = this.created_by;
  // }
  //
  // @OnLoad()
  // convertToDayjs() {
  //   if (this.created_at) this.created_at = dayjs(this.created_at);
  //   if (this.updated_at) this.updated_at = dayjs(this.updated_at);
  //   if (this.deleted_at) this.deleted_at = dayjs(this.deleted_at);
  // }
}
