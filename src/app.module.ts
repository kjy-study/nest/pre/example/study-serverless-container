import { Logger, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { SqlHighlighter } from '@mikro-orm/sql-highlighter';
import { Code } from './entities/code.entity';

const logger = new Logger('MikroORM');

@Module({
  imports: [
    MikroOrmModule.forRoot({
      autoLoadEntities: true,
      dbName: `toy-estate`,
      type: 'mongo',
      timezone: '+09:00',
      clientUrl:
        'mongodb+srv://toy-estate:PFLAqnTVvqq3Hzn@toy-estate.g1t5d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
      highlighter: new SqlHighlighter(),
      debug: true,
      logger: logger.log.bind(logger),
    }),
    MikroOrmModule.forFeature([Code]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
