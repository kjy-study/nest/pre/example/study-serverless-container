import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Server } from 'http';
import { ExpressAdapter } from '@nestjs/platform-express';
import { eventContext } from 'aws-serverless-express/middleware';
import { createServer } from 'aws-serverless-express';

const express = require('express');
let cachedServer: Server;
const binaryMimeTypes: string[] = [];

export async function bootstrap(serverless?: boolean) {
  if (!cachedServer) {
    const expressApp = express();
    const app = await NestFactory.create(
      AppModule,
      new ExpressAdapter(expressApp),
    );

    if (serverless) {
      app.use(eventContext());
      await app.init();
      cachedServer = createServer(expressApp, undefined, binaryMimeTypes);
      return cachedServer;
    } else {
      await app.listen(3000);
    }
  }
  return cachedServer;
}
bootstrap();
