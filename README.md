# AWS Labmda Contianer Example

* MikroORM과 AWS Lambda와 호환이 안되서 `AWS Lambda Container`를 사용한다. 

### serverless 설치
```
npm i -g serverless
```

### 프로젝트 생성
```bash
nest new serverless-contianer-api
```

### AWS 사용자 셋팅
* IAM 계정 생성하여 aws_access_key_id와, aws_secret_access_key를 발급 받습니다.
```bash
cd study-serverless-api

# AWS에서 발급받은 인증 정보를 셋팅 합니다.
sls config credentials --provider aws --key aws_access_key_id --secret aws_secret_access_key
```

###serverless 의존성 설치
```
npm i -s aws-serverless-express aws-lambda
```

### 실행 환경 셋팅
* `main.ts` 파일 하나로 로컬 실행과 labmda container를 실행할 수 있도록 수정 합니다. 

```typescript
// src/main.ts
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Server } from 'http';
import { ExpressAdapter } from '@nestjs/platform-express';
import { eventContext } from 'aws-serverless-express/middleware';
import { createServer } from 'aws-serverless-express';

const express = require('express');
let cachedServer: Server;
const binaryMimeTypes: string[] = [];

export async function bootstrap(serverless?: boolean) {
  if (!cachedServer) {
    const expressApp = express();
    const app = await NestFactory.create(
      AppModule,
      new ExpressAdapter(expressApp),
    );

    if (serverless) {
      app.use(eventContext());
      await app.init();
      cachedServer = createServer(expressApp, undefined, binaryMimeTypes);
      return cachedServer;
    } else {
      await app.listen(3000);
    }
  }
  return cachedServer;
}
bootstrap();
```

* serverless container 환경에서 실행될 수 있도록 lambda.ts 파일을 생성 합니다.
```typescript
// src/lambda.ts
import { Context, Handler } from 'aws-lambda';
import { Server } from 'http';
import { proxy } from 'aws-serverless-express';
import { bootstrap } from './main';

// NOTE: If you get ERR_CONTENT_DECODING_FAILED in your browser, this is likely
// due to a compressed response (e.g. gzip) which has not been handled correctly
// by aws-serverless-express and/or API Gateway. Add the necessary MIME types to
// binaryMimeTypes below

let cachedServer: Server;

export const handler: Handler = async (event: any, context: Context) => {
  cachedServer = await bootstrap(true);
  return proxy(cachedServer, event, context, 'PROMISE').promise;
};
```

* serverless framework 설정파일을 프로젝트 root에 생성합니다.
```yaml
# serverless.yaml
service: study-serverless-container

useDotenv: true

plugins:

provider:
  name: aws
  runtime: nodejs14.x
  region: ap-northeast-1
  stage: ${opt:stage, 'development'}
  ecr:
    # In this section you can define images that will be built locally and uploaded to ECR
    images:
      appimage: # AWS ECR에 업로드되는 이미지 이름 입니다.
        path: ./ # dockerfile의 위치입니다.


functions:
  main:
    image:
      name: appimage # provider에서 정의한 ERC 이미지를 사용하여 함수를 실행합니다.
#      command:
#        - app.greeter
#      entryPoint:
#        - '/lambda-entrypoint.sh'

    timeout: 60
    events:
      - http:
          method: any
          path: /{any+}
      - http:
          method: any
          path: /
    environment:
      NODE_ENV: ${self:provider.stage}
```

