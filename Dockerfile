FROM --platform=linux/amd64 public.ecr.aws/lambda/nodejs:latest as builder

ENV TZ Asia/Seoul

COPY . .

RUN npm ci --only=production

RUN npm run build


FROM --platform=linux/amd64 public.ecr.aws/lambda/nodejs:latest as deploy

COPY --from=builder /var/task/dist ./dist
COPY --from=builder /var/task/node_modules .

CMD ["dist/lambda.handler"]